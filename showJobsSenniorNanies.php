<?php
/*
Plugin Name: show Jobs Sennior Nanies
Plugin URI: 
Description:
Author: LuisMiguelNarvaez
Version: 1.0.3
Author URI: https://gitlab.com/luism23
License: GPL
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/show-jobs-sennior-nannies',
	__FILE__,
	'show-jobs-sennior-nannies'
);
$myUpdateChecker->setAuthentication('glpat-puSVzsHaWSc66qa_rVr1');
$myUpdateChecker->setBranch('master');


function getJobsSenniorNannies()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
	CURLOPT_URL => 'https://careers-api.pllffrd.workers.dev/jobs',
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => '',
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => 'GET',
	));

	$response = curl_exec($curl);

	curl_close($curl);
	return json_decode($response,true);

}

function showJobsSenniorNannies()
{
	$jobs = getJobsSenniorNannies();
	for ($i=0; $i < count($jobs); $i++) { 
		$job = $jobs[$i];
		?>
		<div class="sad">
			<h1 class="title-poster">
				<?=$job["name"]?>
			</h1>
			<p class="text-description">
				<?=$job["description"]?>
			</p>
		</div>
		<?php
	}
}

add_shortcode( 'showJobsSenniorNannies', 'showJobsSenniorNannies' );